<?php
/*
  Template Name: Pirated
*/
get_header();
?>
<?php
while (have_posts()) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
    <?php the_content(); ?> <!-- Page Content -->
<?php
endwhile;
wp_reset_query(); //resetting the page query
?>
<?php
get_footer();
?>