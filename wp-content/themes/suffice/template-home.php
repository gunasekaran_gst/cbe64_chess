<?php
/*
  Template Name: Home
*/
get_header();
?>
<?php
while (have_posts()) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
    <?php the_content(); ?> <!-- Page Content -->
<?php
endwhile;
wp_reset_query(); //resetting the page query
?>
<div class="container-fluid">
    <div class="container">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 padd_left_right_none padd_top_20">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?php echo do_shortcode('[rev_slider alias="newslider20141"]'); ?>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12  padd_left_right_none padd_top_10 ">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12 widget-textarea_right ">
                                <a class="live" href=""> LIVE 19th National Cities Chess Championship </a>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 widget-textarea_right ">
                                <a class="live" href=""> Entries for National Cities Team </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12  padd_top_10">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 widget-textarea_right ">
                            <a class="live" href=""> Live Games 2018 National Under 11 open and girls</a>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_none padd_botton_10">
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 widget-textarea_right padd_left_none ">
                            <a class="live" href="">Solver’s corner # 58 (02.08.2018)</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">

                    <?php $args = array(
                        'posts_per_page' => 4,
                        'offset' => 0,
                        'category' => '',
                        'category_name' => 'Featured News',
                        'orderby' => 'rand',
                        'order' => 'DESC',
                        'include' => '',
                        'exclude' => '',
                        'meta_key' => '',
                        'meta_value' => '',
                        'post_type' => 'post',
                        'post_mime_type' => '',
                        'post_parent' => '',
                        'author' => '',
                        'author_name' => '',
                        'post_status' => 'publish',
                        'suppress_filters' => true,
                        'fields' => '',
                    );
                    $posts_array = get_posts($args);
                    if ($posts_array) {
                        foreach ($posts_array as $post) :setup_postdata($post); ?>

                            <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail'); ?>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 padd_top_20">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none featured_post">
                                    <img src="<?php echo $image[0]; ?>" alt="<?php echo the_title(); ?>">
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_5">
                                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none national"
                                       href="<?php the_permalink(); ?>"><?php the_title(); ?> </a>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none latest-news-meta">
                                    <div><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> <?php echo get_the_date(); ?></span>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_10 latest-news-excerpt">
                                    <p><?php echo wp_strip_all_tags(the_excerpt()); ?></p>
                                </div>
                            </div>
                        <?php
                        endforeach;
                        wp_reset_postdata();
                    }
                    ?>
                </div>


                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 padd_left_none">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  india_top widget-india">
                            India Top Players
                        </div>
                        <table class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_none table table-hover women_top">
                            <tbody>
                            <tr>
                                <th>NAME</th>
                                <th>RATING</th>
                            </tr>
                            <tr>
                                <td class="player-table-value">Anand, Viswanathan</td>
                                <td class="player-table-value">2768</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">Harikrishna, P.</td>
                                <td class="player-table-value">2734</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">Vidit, Santosh Gujrathi</td>
                                <td class="player-table-value">2718</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">Adhiban, B.</td>
                                <td class="player-table-value">2671</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">Sasikiran, Krishnan</td>
                                <td class="player-table-value">2666</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">Sethuraman, S.P.</td>
                                <td class="player-table-value">2657</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">Negi, Parimarjan</td>
                                <td class="player-table-value">2656</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">Ganguly, Surya Shekhar</td>
                                <td class="player-table-value">2652</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">Gupta, Abhijeet</td>
                                <td class="player-table-value">2614</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">Karthikeyan, Murali</td>
                                <td class="player-table-value">2609</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 padd_left_none">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  india_top widget-india">
                            Top Women Players
                        </div>
                        <table class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_none table table-hover women_top">
                            <tbody>
                            <tr>
                                <th>NAME</th>
                                <th>RATING</th>
                            </tr>
                            <tr>
                                <td class="player-table-value">Koneru, Humpy</td>
                                <td class="player-table-value">2557</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">Harika, Dronavalli</td>
                                <td class="player-table-value">2494</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">Tania, Sachdev</td>
                                <td class="player-table-value">2393</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">Karavade, Eesha</td>
                                <td class="player-table-value">2390</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">Soumya, Swaminathan</td>
                                <td class="player-table-value">2361</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">Vijayalakshmi, Subbaraman</td>
                                <td class="player-table-value">2359</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">Padmini, Rout</td>
                                <td class="player-table-value">2338</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">Vaishali R</td>
                                <td class="player-table-value">2337</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">Aakanksha Hagawane</td>
                                <td class="player-table-value">2314</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">Vantika Agrawal</td>
                                <td class="player-table-value">2295</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>


                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_none padd_top_10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_none">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india_anouns">
                                Announcements
                            </div>
                            <hr>
                        </div>


                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">

                            <?php $args = array(
                                'posts_per_page' => 4,
                                'offset' => 0,
                                'category' => '',
                                'category_name' => 'Announcements',
                                'orderby' => 'rand',
                                'order' => 'DESC',
                                'include' => '',
                                'exclude' => '',
                                'meta_key' => '',
                                'meta_value' => '',
                                'post_type' => 'post',
                                'post_mime_type' => '',
                                'post_parent' => '',
                                'author' => '',
                                'author_name' => '',
                                'post_status' => 'publish',
                                'suppress_filters' => true,
                                'fields' => '',
                            );
                            $posts_array = get_posts($args);
                            if ($posts_array) {
                                foreach ($posts_array as $post) :setup_postdata($post); ?>

                                    <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail'); ?>

                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_10">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_10 latest-news-excerpt">
                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 padd_left_right_none ">
                                                <img class="announce_img" src="<?php echo $image[0]; ?>">
                                            </div>

                                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12  padd_left_right_none">
                                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none national"
                                                   href="<?php the_permalink(); ?>"><?php the_title(); ?> </a>
                                                <div><span class="latest-news-date"><i
                                                                class="fa fa-clock-o"></i> <?php echo get_the_date(); ?></span>
                                                </div>

                                            </div>


                                        </div>

                                    </div>


                                <?php
                                endforeach;
                                wp_reset_postdata();
                            }
                            ?>
                        </div>

                        <hr>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  padd_botton_30 padd_top_20">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_none">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_none padd_left_none">
                            <iframe class="chess_video"
                                    src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fdelhichess%2Fvideos%2F1671643356217686%2F&amp;show_text=0&amp;width=500"
                                    scrolling="no" allowfullscreen="allowfullscreen" width="500" height="440"
                                    frameborder="0"></iframe>
                            <!--                                <iframe class="iframe" src="https://www.youtube.com/embed/1TlKSsgMbdE width=800" width="800" height="440"></iframe>-->
                        </div>
                    </div>
                </div>


                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">

                    <?php $args = array(
                        'posts_per_page' => 4,
                        'offset' => 0,
                        'category' => '',
                        'category_name' => 'Generalnews',
                        'orderby' => 'rand',
                        'order' => 'DESC',
                        'include' => '',
                        'exclude' => '',
                        'meta_key' => '',
                        'meta_value' => '',
                        'post_type' => 'post',
                        'post_mime_type' => '',
                        'post_parent' => '',
                        'author' => '',
                        'author_name' => '',
                        'post_status' => 'publish',
                        'suppress_filters' => true,
                        'fields' => '',
                    );
                    $posts_array = get_posts($args);
                    if ($posts_array) {
                        foreach ($posts_array as $post) :setup_postdata($post); ?>

                            <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail'); ?>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 padd_top_20">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none featured_post">
                                    <img src="<?php echo $image[0]; ?>" alt="<?php echo the_title(); ?>">
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_5">
                                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none national"
                                       href="<?php the_permalink(); ?>"><?php the_title(); ?> </a>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none latest-news-meta">
                                    <div><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> <?php echo get_the_date(); ?></span>
                                    </div>
                                </div>
                            </div>
                        <?php
                        endforeach;
                        wp_reset_postdata();
                    }
                    ?>
                </div>

            </div>


            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none padd_top_20">
                <div class="widget-textarea padd_left_right_none col-lg-12 padd_botton_10 padd_top_10 col-md-12 col-sm-12 col-xs-12">
                    <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                        <img src="http://wordpress.lan/wp-content/uploads/2018/08/player1.png"
                             class="player_img" alt="player">
                    </div>
                    <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                        <a class="player_search" href="http://wordpress.lan/player-search/"> Player Search </a>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                </div>
                <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                        <img src="http://wordpress.lan/wp-content/uploads/2018/08/chess_schools.png"
                             class="player_img" alt="player">
                    </div>
                    <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                        <a class="player_search" href="http://cis.fide.com/"> Chess in Schools</a>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                </div>
                <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                        <img src="http://wordpress.lan/wp-content/uploads/2018/08/cash.png"
                             class="player_img" alt="player">
                    </div>
                    <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                        <a class="player_search" href="http://wordpress.lan/cashawards/"> Cash Awards</a>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                </div>
                <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                        <img src="http://wordpress.lan/wp-content/uploads/2018/08/rating.png"
                             class="player_img" alt="player">
                    </div>
                    <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                        <a class="player_search" href="http://wordpress.lan/ratingquery/">Rating Query Online</a>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                </div>
                <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                        <img src="http://wordpress.lan/wp-content/uploads/2018/08/arbiter.png"
                             class="player_img" alt="player">
                    </div>
                    <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                        <a class="player_search" href="http://wordpress.lan/arbitercorner/">Arbiter Corner</a>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                </div>
                <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                        <img src="http://wordpress.lan/wp-content/uploads/2018/08/aicf-top.png"
                             class="player_img" alt="player">
                    </div>
                    <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                        <a class="player_search" href="http://wordpress.lan/oci-card-holder/">PIO / OCI card
                            holders</a>
                    </div>
                </div>


                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">

                    <?php $args = array(
                        'posts_per_page' => 1,
                        'offset' => 0,
                        'category' => '',
                        'category_name' => 'hurry',
                        'orderby' => 'rand',
                        'order' => 'DESC',
                        'include' => '',
                        'exclude' => '',
                        'meta_key' => '',
                        'meta_value' => '',
                        'post_type' => 'post',
                        'post_mime_type' => '',
                        'post_parent' => '',
                        'author' => '',
                        'author_name' => '',
                        'post_status' => 'publish',
                        'suppress_filters' => true,
                        'fields' => '',
                    );
                    $posts_array = get_posts($args);
                    if ($posts_array) {
                        foreach ($posts_array as $post) :setup_postdata($post); ?>

                            <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail'); ?>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_top_20 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="<?php echo $image[0]; ?>" alt="<?php echo the_title(); ?>">
                                </div>
                            </div>
                        <?php
                        endforeach;
                        wp_reset_postdata();
                    }
                    ?>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                        <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live"
                           href="http://wordpress.lan/national-reg/">NATIONAL TOURNAMENT – <br>24 x 7 assistance is
                            arranged towards confirmation of <br> entry : Phone numbers : +917358534422 /
                            +918610193178</a></div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_5">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                        <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live"
                           href="http://wordpress.lan/aicf-payments/"> CLICK HERE FOR VARIOUS
                            PAYMENTS </a>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                        <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live"
                           href="http://wordpress.lan/faq/">FAQ</a>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_10">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                        News
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">

                    <?php $args = array(
                        'posts_per_page' => 3,
                        'offset' => 0,
                        'category' => '',
                        'category_name' => 'news',
                        'orderby' => 'rand',
                        'order' => 'DESC',
                        'include' => '',
                        'exclude' => '',
                        'meta_key' => '',
                        'meta_value' => '',
                        'post_type' => 'post',
                        'post_mime_type' => '',
                        'post_parent' => '',
                        'author' => '',
                        'author_name' => '',
                        'post_status' => 'publish',
                        'suppress_filters' => true,
                        'fields' => '',
                    );
                    $posts_array = get_posts($args);
                    if ($posts_array) {
                        foreach ($posts_array as $post) :setup_postdata($post); ?>

                            <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail'); ?>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_10">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_10 latest-news-excerpt">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none ">
                                        <img src="<?php echo $image[0]; ?>">
                                    </div>
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12  padd_left_right_none">
                                    </div>

                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12  padd_left_right_none">
                                        <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none national"
                                           href="<?php the_permalink(); ?>"><?php the_title(); ?> </a>
                                        <div><span class="latest-news-date"><i
                                                        class="fa fa-clock-o"></i> <?php echo get_the_date(); ?></span>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        <?php
                        endforeach;
                        wp_reset_postdata();
                    }
                    ?>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                        Featured Events
                    </div>
                </div>
                <?php
                // First lets set some arguments for the query:
                // Optionally, those could of course go directly into the query,
                // especially, if you have no others but post type.
                $args = array(
                    'post_type' => 'events',
                    'orderby' => 'rand',
                    'posts_per_page' => 4
                    // Several more arguments could go here. Last one without a comma.
                );
                // Query the posts:
                $featured_query = new WP_Query($args);

                // Loop through the obituaries:
                while ($featured_query->have_posts()) : $featured_query->the_post(); ?>
                    <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail'); ?>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_5">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <img src="<?php echo $image[0]; ?>">
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title"
                               href="<?php the_permalink(); ?>">
                                <?php the_title(); ?></a>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                <div class="news-date-meta"><span class="latest-news-date"><i
                                                class="fa fa-clock-o"></i> <?php echo get_the_date(); ?></span></div>
                            </div>
                        </div>
                    </div>

                <?php endwhile;
                wp_reset_postdata(); ?>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                        <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live"
                           href="https://ratings.fide.com/title_applications.phtml">Title Applications</a>
                    </div>
                </div>

                <div class="widget-textareass padd_left_right_none col-lg-12 padd_botton_10 padd_top_10 col-md-12 col-sm-12 col-xs-12">
                    <div class="padd_left_right_none  col-lg-1 col-md-1 col-sm-1 col-xs-12">
                        <img src="http://wordpress.lan/wp-content/uploads/2018/08/india.png"
                             class="player_img" alt="player">
                    </div>
                    <div class=" col-lg-10 col-md-10 col-sm-10 col-xs-12 padd_left_right_none">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 player_searchss"> INDIA HIGHLIGHTS</div>
                    </div>
                    <div class="padd_left_right_none  col-lg-1 col-md-1 col-sm-1 col-xs-12">
                        <img src="http://wordpress.lan/wp-content/uploads/2018/08/india.png"
                             class="player_img" alt="player">
                    </div>
                </div>


                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                        <div id="circle">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <strong>76612</strong>
                            </div>
                            <span>Registered Players</span>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                        <div id="circle">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <strong>28574</strong>
                            </div>
                            <span>FIDE Rated Players</span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                        <div id="circle">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <strong>219</strong>
                            </div>
                            <span>Tournaments in 2018</span>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                        <div id="circle">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <strong>51</strong>
                            </div>
                            <span> Grand Masters </span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_20">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                        <div id="circle">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_20">
                                <strong>101</strong>
                            </div>
                            <span> International Masters </span>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                        <div id="circle">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_20">
                                <strong>7</strong>
                            </div>
                            <span> Women Grand Masters </span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                        AICF Chronicles
                    </div>
                </div>


                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">

                    <?php $args = array(
                        'posts_per_page' => 1,
                        'offset' => 0,
                        'category' => '',
                        'category_name' => 'Chronicles',
                        'orderby' => 'rand',
                        'order' => 'DESC',
                        'include' => '',
                        'exclude' => '',
                        'meta_key' => '',
                        'meta_value' => '',
                        'post_type' => 'post',
                        'post_mime_type' => '',
                        'post_parent' => '',
                        'author' => '',
                        'author_name' => '',
                        'post_status' => 'publish',
                        'suppress_filters' => true,
                        'fields' => '',
                    );
                    $posts_array = get_posts($args);
                    if ($posts_array) {
                    foreach ($posts_array

                    as $post) :
                    setup_postdata($post); ?>

                    <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail'); ?>


                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10 events-titless " href="">
                        <?php echo get_the_date(); ?></a>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                            <a href="<?php the_permalink(); ?>"> <img src="<?php echo $image[0]; ?>"> </a>
                        </div>
                        <?php
                        endforeach;
                        wp_reset_postdata();
                        }
                        ?>
                    </div>

                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">

                    <?php $args = array(
                        'posts_per_page' => 1,
                        'offset' => 0,
                        'category' => '',
                        'category_name' => 'meta',
                        'orderby' => 'rand',
                        'order' => 'DESC',
                        'include' => '',
                        'exclude' => '',
                        'meta_key' => '',
                        'meta_value' => '',
                        'post_type' => 'post',
                        'post_mime_type' => '',
                        'post_parent' => '',
                        'author' => '',
                        'author_name' => '',
                        'post_status' => 'publish',
                        'suppress_filters' => true,
                        'fields' => '',
                    );
                    $posts_array = get_posts($args);
                    if ($posts_array) {
                    foreach ($posts_array

                    as $post) :
                    setup_postdata($post); ?>

                    <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail'); ?>


                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10 events-titless " href="">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                                Mate in two moves
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <a href="<?php the_permalink(); ?>"> <img src="<?php echo $image[0]; ?>"> </a>
                            </div>
                            <?php
                            endforeach;
                            wp_reset_postdata();
                            }
                            ?>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    get_footer();
    ?>


