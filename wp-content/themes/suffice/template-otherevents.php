<?php
/*
  Template Name: Other Events
*/
get_header();
?>
<?php
while (have_posts()) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
    <?php the_content(); ?> <!-- Page Content -->
<?php
endwhile;
wp_reset_query(); //resetting the page query
?>
<div class="container-fluid">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_none">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padd_left_none announce">
                                Other Events
                            </div>
                            <hr>
                        </div>

                        <table class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_none table table-hover">
                            <tbody>
                            <tr>
                                <th>Title</th>
                                <th>Date</th>
                                <th>Location</th>
                                <th></th>
                            </tr>
                            <tr>
                                <td><a class="player-table-values" href=""
                                       rel="bookmark"
                                       title="11th Modern School FIDE Rating chess tournament for school children – 190868 / TN / 2018">11th
                                        Modern School FIDE Rating chess tournament for school children - 190868 / TN /
                                        2018</a></td>
                                <td class="date">03 Aug - 06 Aug</td>
                                <td class="location">Modern Senior Secondary School,AG's Office Colony,
                                    Nanganallur,Chennai,TN,600061
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>


                            <tr>
                                <td><a class="player-table-values" href="" rel="bookmark"
                                       title="MNM – CPA – Jaipur Cup – 192228 / RAJ / 2018">MNM - CPA - Jaipur Cup -
                                        192228 / RAJ / 2018</a></td>
                                <td class="date">09 Aug - 11 Aug</td>
                                <td class="location">3ctidd Sector 2, Vidhyadhar Nagar, Jaipur</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href="" title="Brochure"
                                            download=""><i class="fa fa-download" aria-hidden="true"></i></a><a></a>
                                </td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                       rel="bookmark"
                                       title="AICFB East Zone Rating Chess Tournament for the Visually Challenged – 193056 / AICFB / 2018">AICFB
                                        East Zone Rating Chess Tournament for the Visually Challenged - 193056 / AICFB /
                                        2018</a></td>
                                <td class="date">09 Aug - 12 Aug</td>
                                <td class="location">Ashalata Viklang Vikas Kendra,Sector 5, Bokaro Steel City,Bokaro,
                                    Jharkhand,827006
                                </td>
                                <td class="url"><a href="" title="Email"><i class="fa fa-envelope"
                                                                                                  aria-hidden="true"></i>
                                    </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                       rel="bookmark"
                                       title="Maharashtra State Open Under – 25 Youth – 191896 / MAH(S) / 2018">Maharashtra
                                        State Open Under - 25 Youth - 191896 / MAH(S) / 2018</a></td>
                                <td class="date">09 Aug - 13 Aug</td>
                                <td class="location">Shree Sant Narahari Maharaj Mangal Karyalaya, Near Purushottam
                                    School, Jail Road, Nasik - 422101
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                       rel="bookmark" title="2nd Sri Raghavendra Open FIDE Rated – 183087 / TN / 2018">2nd
                                        Sri Raghavendra Open FIDE Rated - 183087 / TN / 2018</a></td>
                                <td class="date">09 Aug - 12 Aug</td>
                                <td class="location">Sri Ganesh Thirumana Mandapam,Pammal,Chennai,TN,600075</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                       rel="bookmark" title="3rd MMCA Open FIDE Rated – 192234 / KER  /2018">3rd MMCA
                                        Open FIDE Rated - 192234 / KER /2018</a></td>
                                <td class="date">11 Aug - 14 Aug</td>
                                <td class="location">CMS College Kottayam,Kerala</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href="" title="Brochure"
                                            download=""><i class="fa fa-download" aria-hidden="true"></i></a><a></a>
                                </td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                       rel="bookmark"
                                       title="3rd Mother Chess Open FIDE Rating chess tournament – 190463 / AP / 2018">3rd
                                        Mother Chess Open FIDE Rating chess tournament - 190463 / AP / 2018</a></td>
                                <td class="date">12 Aug - 15 Aug</td>
                                <td class="location">Welfare Institute of Science Technology &amp; Management college,
                                    Pinagadi, Near Pendurthi, Visakhapatnam, Andhra Pradesh – 530047, INDIA
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                       rel="bookmark" title="1st RCPA All India Open FIDE Rating – 192299 / GUJ / 2018">1st
                                        RCPA All India Open FIDE Rating - 192299 / GUJ / 2018</a></td>
                                <td class="date">14 Aug - 19 Aug</td>
                                <td class="location">Anandnagar community Hall Shopping Centre,anandnagar Main road,
                                    Gayatri Nagar, Geetanjali Park, Bhaktinagar,Rajkot,GJ,360002
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href="" title="Brochure"
                                            download=""><i class="fa fa-download" aria-hidden="true"></i></a><a></a>
                                </td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                       rel="bookmark"
                                       title="All India Open FIDE Rating Chess Tournament – 2018  – 193810 / MAH / 2018">All
                                        India Open FIDE Rating Chess Tournament - 2018 - 193810 / MAH / 2018</a></td>
                                <td class="date">16 Aug - 19 Aug</td>
                                <td class="location">Ram Mandir Trust Hall, Janata Colony, Jogeshwari East Mumbai
                                    -400060
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            download=""><i class="fa fa-download" aria-hidden="true"></i></a><a></a>
                                </td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                       rel="bookmark" title="NC Chummar Memorial FIDE Rated Open – 191664 / KER / 2018">NC
                                        Chummar Memorial FIDE Rated Open - 191664 / KER / 2018</a></td>
                                <td class="date">19 Aug - 22 Aug</td>
                                <td class="location">Paramekkav Vidya Mandir, MLA Road, Thrissur - 2 Kerala</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href="" title="Brochure"
                                            download=""><i class="fa fa-download" aria-hidden="true"></i></a><a></a>
                                </td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                       rel="bookmark"
                                       title="Samreddyagi Reddy Centenary Celebrations All India Open FIDE – 183455 / TEL / 2018">Samreddyagi
                                        Reddy Centenary Celebrations All India Open FIDE - 183455 / TEL / 2018</a></td>
                                <td class="date">19 Aug - 23 Aug</td>
                                <td class="location">Mahaveer Institute of Science
                                    &amp;Technology,Bandlaguda,Hyderabad
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href="" title="Brochure"
                                            download=""><i class="fa fa-download" aria-hidden="true"></i></a><a></a>
                                </td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                       rel="bookmark" title="4th ADRA FIDE Rated Open – 2018  – 190850 / WB / 2018">4th
                                        ADRA FIDE Rated Open - 2018 - 190850 / WB / 2018</a></td>
                                <td class="date">21 Aug - 25 Aug</td>
                                <td class="location">Railway Community hall, S.E.Railway, Adra</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                       rel="bookmark"
                                       title="Maharashtra State Selection U-17 open &amp;  girls fide rating chess championship 2018 – 194564 / 194565 /MAH(S) /2018">Maharashtra
                                        State Selection U-17 open &amp; girls fide rating chess championship 2018 -
                                        194564 / 194565 /MAH(S) /2018</a></td>
                                <td class="date">21 Aug - 24 Aug</td>
                                <td class="location">Indian medical association hall,civil lines Akola 444001</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                       rel="bookmark"
                                       title="Samreddyagi Reddy Centenary Celebrations All India below 1500 FIDE – 183466 / TEL / 2018">Samreddyagi
                                        Reddy Centenary Celebrations All India below 1500 FIDE - 183466 / TEL / 2018</a>
                                </td>
                                <td class="date">24 Aug - 26 Aug</td>
                                <td class="location">Mahaveer Institute of Science
                                    &amp;Technology,Bandlaguda,Hyderabad
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href="" title="Brochure"
                                            download=""><i class="fa fa-download" aria-hidden="true"></i></a><a></a>
                                </td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                       rel="bookmark"
                                       title="Netaji Subhas Chandra Bose – Pasumpon Muthuramalingam Thevar  FIDE rated  below 1600 – 190848 / TN / 2018">Netaji
                                        Subhas Chandra Bose - Pasumpon Muthuramalingam Thevar FIDE rated below 1600 -
                                        190848 / TN / 2018</a></td>
                                <td class="date">24 Aug - 26 Aug</td>
                                <td class="location">Jawaharlal Nehru Stadium,Periamet, Chennai,600003</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href="" title="Brochure"
                                            download=""><i class="fa fa-download" aria-hidden="true"></i></a><a></a>
                                </td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                       rel="bookmark" title="Albertian open FIDE Rating – 193057 / KER / 2018">Albertian
                                        open FIDE Rating - 193057 / KER / 2018</a></td>
                                <td class="date">24 Aug - 28 Aug</td>
                                <td class="location">St.Alberts' College (Autonomous), Banerji Road, ernakulam, Kerala
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href=""
                                            title="Brochure" download=""><i class="fa fa-download"
                                                                            aria-hidden="true"></i></a><a></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                       rel="bookmark" title="3rd Kharagpur classic 2018 –  190751 / WB / 2018">3rd
                                        Kharagpur classic 2018 - 190751 / WB / 2018</a></td>
                                <td class="date">27 Aug - 01 Sep</td>
                                <td class="location">Kharagpur Yuba Sangha Club,Malancha, Kharagpur,WB</td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href="" title="Brochure"
                                            download=""><i class="fa fa-download" aria-hidden="true"></i></a><a></a>
                                </td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href=""
                                       rel="bookmark"
                                       title="ARCA State Under 19 Boys and Girls FIDE Rated – 189557 / RAJ (S) / 2018">ARCA
                                        State Under 19 Boys and Girls FIDE Rated - 189557 / RAJ (S) / 2018</a></td>
                                <td class="date">31 Aug - 02 Sep</td>
                                <td class="location">Maa karma sahu dham, near devendra dham, front of celebration mall
                                    udaipur(Rajasthan)
                                </td>
                                <td class="url"><a href="" title="Email"><i
                                                class="fa fa-envelope" aria-hidden="true"></i> </a><a class="down"
                                            href="" title="Brochure"
                                            download=""><i class="fa fa-download" aria-hidden="true"></i></a><a></a>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none padd_top_20">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="">ALL EVENTS</a>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_15">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="">OFFICIAL EVENTS</a>
                        </div>
                    </div>
                    <form>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group padd_left_right_none padd_top_20">
                            <select class="form-control" id="sel1">
                                <option value="0">Select a Reg Status</option>
                                <option value="y">Active</option>
                                <option value="p">Processing</option>
                                <option value="n">Inactive</option>
                            </select>
                            <button type="button" class="btn btn-warning">Filter</button>
                            <input class="btn btn-info" onclick="loadpage();" value="Clear Filter" name="clear"
                                   class="event_filter_button"
                                   type="reset">
                        </div>
                    </form>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                        <div class="widget-textarea padd_left_right_none col-lg-12 padd_botton_10 padd_top_10 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/player1.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://wordpress.lan/player-search/"> Player Search </a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/chess_schools.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href=""> Chess in Schools</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/cash.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://wordpress.lan/cashawards/"> Cash Awards</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/rating.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://wordpress.lan/ratingquery/">Rating Query Online</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/arbiter.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://wordpress.lan/arbitercorner/">Arbiter Corner</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/aicf-top.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://wordpress.lan/oci-card-holder/">PIO / OCI card holders</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  padd_left_right_none padd_botton_10 padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <a href="http://wordpress.lan/twin-international/"> <img src="http://wordpress.lan/wp-content/uploads/2018/08/goa.jpg" alt="player"> </a>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="http://wordpress.lan/national-reg/">NATIONAL TOURNAMENT – <br>24 x 7 assistance is arranged towards confirmation of <br> entry : Phone numbers : +917358534422 / +918610193178</a> </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_5">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="http://wordpress.lan/aicf-payments/"> CLICK HERE FOR VARIOUS
                                    PAYMENTS </a>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="http://wordpress.lan/faq/">FAQ</a>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_10">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                                News
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://wordpress.lan/wp-content/uploads/2018/08/news.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Under-11: Shreyash and Savitha </a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://wordpress.lan/wp-content/uploads/2018/08/news1.jpeg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href=""> National Under-07: Amogh and Lakshana </a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12
                             latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://wordpress.lan/wp-content/uploads/2018/08/thumbnail.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Under-15: Ajay, Divya crowned </a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                                Featured Events
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_5">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://wordpress.lan/wp-content/uploads/2018/08/features4.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href=""> 32nd
                                    National Under – 11 (Open &amp; Girls)</a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://wordpress.lan/wp-content/uploads/2018/08/features3.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Cities – 2018  – 189826 / WB / 2018</a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://wordpress.lan/wp-content/uploads/2018/08/features2.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Under – 25 <br> (Open  &amp; Girls) – 2018</a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://wordpress.lan/wp-content/uploads/2018/08/features1.png"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">Goa International GM Open 2018 – 186616</a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="">Title Applications</a>
                            </div>
                        </div>

                        <div class="widget-textareass padd_left_right_none col-lg-12 padd_botton_10 padd_top_10 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/india.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class=" col-lg-10 col-md-10 col-sm-10 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 player_searchss"> INDIA HIGHLIGHTS </div>
                            </div>
                            <div class="padd_left_right_none  col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/india.png"
                                     class="player_img" alt="player">
                            </div>
                        </div>


                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <strong>76612</strong>
                                    </div>
                                    <span>Registered Players</span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <strong>28574</strong>
                                    </div>
                                    <span>FIDE Rated Players</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <strong>219</strong>
                                    </div>
                                    <span>Tournaments in 2018</span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <strong>51</strong>
                                    </div>
                                    <span> Grand Masters </span>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_20">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_20">
                                        <strong>101</strong>
                                    </div>
                                    <span> International Masters </span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_20">
                                        <strong>7</strong>
                                    </div>
                                    <span> Women Grand Masters </span>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                                AICF Chronicles
                            </div>
                        </div>
                        <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10 events-titless " href="">July 2018</a>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/AICF.jpg">
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                                Mate in two moves
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/chee_buttom.jpg">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<?php
get_footer();
?>