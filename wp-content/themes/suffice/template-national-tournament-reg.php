<?php
/*
  Template Name: National-reg
*/
get_header();
?>
<?php
while (have_posts()) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
    <?php the_content(); ?> <!-- Page Content -->
<?php
endwhile;
wp_reset_query(); //resetting the page query
?>
    <div class="container-fluid">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_none announce">
                        National Tournament Registration
                    </div>
                    <hr>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20 national_reg">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            Enter the Name:
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                            <select class="form-control" id="sel1">
                                <option value="0">Select Tournament</option>
                                <option value="a">  National Under - 25 ( Open &amp; Girls ) - 2018 - 192951 / MP / 2018</option>
                                <option value="b"> 29thNational Under-17 Open &amp;Girls Chess Championships-2018 - 191608/191609/HAR/2018</option>
                                <option value="c">32nd National Under-9 OPEN &amp; GIRLS CHESS CHAMPIONSHIP 2018 - 191614 / 191615 / JHAR
                                    /2018</option>
                            </select>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                        </div>
                    </div>
  </div>

            </div>
        </div>
    </div>
<?php
get_footer();
?>