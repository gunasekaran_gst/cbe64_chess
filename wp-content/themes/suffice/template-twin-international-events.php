<?php
/*
  Template Name: Twin international
*/
get_header();
?>
<?php
while (have_posts()) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
    <?php the_content(); ?> <!-- Page Content -->
<?php
endwhile;
wp_reset_query(); //resetting the page query
?>
<div class="container-fluid">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_none announce">
                            Twin International events in India – Gujarat and Goa
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none latest-news-meta padd_top_10 padd_botton_10">
                            <div class="news-date-meta"><span class="latest-news-date"><i
                                            class="fa fa-clock-o"></i> May 21, 2018</span></div>
                        </div>

                        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <img src="http://wordpress.lan/wp-content/uploads/2018/08/Announcement3.png"
                                 alt="player">
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_10 latest-news-excerpt">

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <p class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">Players are
                                    request to send your entries to twin International events being organised at Gujarat
                                    and Goa</p>

                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_5">
                            for more details : for <a class="player-table-values" download="" href="">Gujarat event</a>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_botton_20 padd_top_5">
                            and <a class="player-table-values" download="" href=""> Goa event</a>
                        </div>

                        <footer class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_5 entry-footer">
                                    <span class="cat-links">
                                        <a class="chash_awards" href="" rel="category tag"> Featured,  </a>
                                          <a class="chash_awards" href="" rel="category tag">   Flash News,  </a>
                                          <a class="chash_awards" href="" rel="category tag">  Main,   </a>
                                          <a class="chash_awards" href="" rel="category tag">  Slider News,   </a>
                                          <a class="chash_awards" href="" rel="category tag">  Tournament News  </a>
                                    </span>&nbsp;&nbsp;
                        </footer>
                        <hr>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_5">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12  padd_top_10">
                                <a class="button_cash" href="http://wordpress.lan/anand-finishes-third/">
                                    <button type="button" class="btn btn-primary btn-block"> << GM Adam Tukhaev emerges sole leader in LIC 3rd Kolkata  Open


                                    </button>
                                </a>

                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12  padd_top_10 padd_botton_20">
                                <a class="button_cash" href="">
                                    <button type="button" class="btn btn-primary btn-block">GM Srinath Narayanan wins LIC Kolkata  Open
                                        >>
                                    </button>
                                </a>
                            </div>

                        </div>


                    </div>
                </div>


                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                        <div class="widget-textarea padd_left_right_none col-lg-12 padd_botton_10 padd_top_10 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/player1.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://wordpress.lan/player-search/"> Player Search </a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/chess_schools.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href=""> Chess in Schools</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/cash.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://wordpress.lan/cashawards/"> Cash Awards</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/rating.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://wordpress.lan/ratingquery/">Rating Query Online</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/arbiter.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://wordpress.lan/arbitercorner/">Arbiter Corner</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/aicf-top.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://wordpress.lan/oci-card-holder/">PIO / OCI card holders</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  padd_left_right_none padd_botton_10 padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <a href="http://wordpress.lan/twin-international/"> <img src="http://wordpress.lan/wp-content/uploads/2018/08/goa.jpg" alt="player"> </a>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="http://wordpress.lan/national-reg/">NATIONAL TOURNAMENT – <br>24 x 7 assistance is arranged towards confirmation of <br> entry : Phone numbers : +917358534422 / +918610193178</a> </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_5">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="http://wordpress.lan/aicf-payments/"> CLICK HERE FOR VARIOUS
                                    PAYMENTS </a>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="http://wordpress.lan/faq/">FAQ</a>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_10">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                                News
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://wordpress.lan/wp-content/uploads/2018/08/news.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Under-11: Shreyash and Savitha </a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://wordpress.lan/wp-content/uploads/2018/08/news1.jpeg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href=""> National Under-07: Amogh and Lakshana </a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12
                             latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://wordpress.lan/wp-content/uploads/2018/08/thumbnail.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Under-15: Ajay, Divya crowned </a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                                Featured Events
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_5">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://wordpress.lan/wp-content/uploads/2018/08/features4.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href=""> 32nd
                                    National Under – 11 (Open &amp; Girls)</a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://wordpress.lan/wp-content/uploads/2018/08/features3.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Cities – 2018  – 189826 / WB / 2018</a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://wordpress.lan/wp-content/uploads/2018/08/features2.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Under – 25 <br> (Open  &amp; Girls) – 2018</a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://wordpress.lan/wp-content/uploads/2018/08/features1.png"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">Goa International GM Open 2018 – 186616</a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="">Title Applications</a>
                            </div>
                        </div>

                        <div class="widget-textareass padd_left_right_none col-lg-12 padd_botton_10 padd_top_10 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/india.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class=" col-lg-10 col-md-10 col-sm-10 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 player_searchss"> INDIA HIGHLIGHTS </div>
                            </div>
                            <div class="padd_left_right_none  col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/india.png"
                                     class="player_img" alt="player">
                            </div>
                        </div>


                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <strong>76612</strong>
                                    </div>
                                    <span>Registered Players</span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <strong>28574</strong>
                                    </div>
                                    <span>FIDE Rated Players</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <strong>219</strong>
                                    </div>
                                    <span>Tournaments in 2018</span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <strong>51</strong>
                                    </div>
                                    <span> Grand Masters </span>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_20">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_20">
                                        <strong>101</strong>
                                    </div>
                                    <span> International Masters </span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_20">
                                        <strong>7</strong>
                                    </div>
                                    <span> Women Grand Masters </span>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                                AICF Chronicles
                            </div>
                        </div>
                        <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10 events-titless " href="">July 2018</a>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/AICF.jpg">
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                                Mate in two moves
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/chee_buttom.jpg">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<?php
get_footer();
?>