<?php
/*
  Template Name:  FIDE Arbiter Seminar
*/
get_header();
?>
<?php
while (have_posts()) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
    <?php the_content(); ?> <!-- Page Content -->
<?php
endwhile;
wp_reset_query(); //resetting the page query
?>
<div class="container-fluid">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 padd_left_right_none padd_top_20 ">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_none announce">
                            FIDE Arbiter Seminar 2018
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none latest-news-meta padd_top_10 padd_botton_10">
                            <div class="news-date-meta"><span class="latest-news-date"><i
                                            class="fa fa-clock-o"></i> July 24, 2018 </span></div>
                        </div>

                        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_top_20">
                            <img src="http://wordpress.lan/wp-content/uploads/2018/08/Announcement1.jpg"
                                 alt="player">
                        </div>

                        <div id="faq-registration" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_none announcess padd_botton_20 padd_top_10">
                            From 08 to 10 September,2018
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                            <p class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">The FIDE was
                                awarding FIDE arbiters title for arbiters who had the requisite number of norms. But, as
                                per the new regulations of FIDE, it is mandatory for all FIDE Arbiter title seekers to
                                attend the Arbiters Seminar conducted by the FIDE and to pass the exam. As requested by
                                Telangana State Chess Association, AICF has allotted and to conduct an Arbiters Seminar
                                at Hyderabad from 08 to 10 September,2018. Anybody who has basic knowledge about the
                                game can appear for the Seminar.</p>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                            <p class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_10">The AICF
                                The Lecturer will be IA Prof. Rathinam Anantharam (IND), Councilor of the FIDE Arbiters’
                                Commission and FIDE Lecturer and Assistant Lecturer will be IA R. Srivatsan (IND) will
                                conduct the Seminar. All those who are interested in attending the course are requested
                                to transfer the necessary payment using our online portal :
                                http://aicf.in/other-payments/ select accounting head FIDE arbiter seminar. The fee for
                                the seminar is Rs.12,000/- ( with out lodging ) Rs.15,000 (with lodging in sharing
                                basis) which includes FIDE Fee, Exam fee, and study Materials and other expenses
                                connected with the seminar. Interested candidates are requested to complete the
                                registration on or before 03 Sep, 2018. The entries are subject to approval of the
                                concerned State Chess Associations so as to reach AICF before 03 Sep,2018.</p>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                            <p class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_10">
                                Arrival & Registration            :       08 September, 2018  before    : 09:30.A.M.
                            </p>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                            <p class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_10">
                                Departure                                  :       10 September, 2018 after        : 19: 00 hrs.
                            </p>
                        </div>

                        <div id="faq-registration" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_none announcess padd_botton_20 padd_top_10">
                            Telangana State Chess Association, Flat F & G SAPHIRE COMPLEX, Fatehmaidan, Hyderabad-500001
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                            <p class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_10">
                                Detailed Schedule will be informed at the Seminar hall. </p>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                            <p class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_10">
                                For eligibility details please logon to :   <a class="player-table-values"  download="" href=""> FIDE

                                    Click here to download the course-material </a>
                            </p>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                            <p class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_10">
                                Further details please contact : Telangana State Chess Association , Mr.K Sivaprasad General Secretary 403, Akshaya Apartments, Somajiguda, Khairtabad, Hyderabad -500 082 email id : ksprasad.tsca@gmail.com
                                Phone : 7337399299 / 7337578899 http://chesstelangana.com/
                            </p>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                            <p class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_10">
                                Bharat Singh Chauhan
                            </p>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                            <p class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_10">
                                Hon.Secretary
                            </p>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                            <p class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_10">
                                All India Chess Federation
                            </p>
                        </div>

                        <hr>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_5">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12  padd_top_10">
                                <a class="button_cash" href="http://wordpress.lan/anand-finishes-third/">
                                    <button type="button" class="btn btn-primary btn-block"> << National Cities Team Chess Championship 2018
                                    </button>
                                </a>

                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12  padd_top_10 padd_botton_20">
                                <a class="button_cash" href="">
                                    <button type="button" class="btn btn-primary btn-block"> India vs. Spain friendly training camp begins in Kochi
                                        >>
                                    </button>
                                </a>
                            </div>

                        </div>


                    </div>


                </div>


                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none padd_top_20">
                    <div class="widget-textarea padd_left_right_none col-lg-12 padd_botton_10 padd_top_10 col-md-12 col-sm-12 col-xs-12">
                        <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <img src="http://wordpress.lan/wp-content/uploads/2018/08/player1.png"
                                 class="player_img" alt="player">
                        </div>
                        <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                            <a class="player_search" href="http://wordpress.lan/player-search/"> Player Search </a>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                    </div>
                    <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <img src="http://wordpress.lan/wp-content/uploads/2018/08/chess_schools.png"
                                 class="player_img" alt="player">
                        </div>
                        <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                            <a class="player_search" href=""> Chess in Schools</a>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                    </div>
                    <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <img src="http://wordpress.lan/wp-content/uploads/2018/08/cash.png"
                                 class="player_img" alt="player">
                        </div>
                        <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                            <a class="player_search" href="http://wordpress.lan/cashawards/"> Cash Awards</a>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                    </div>
                    <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <img src="http://wordpress.lan/wp-content/uploads/2018/08/rating.png"
                                 class="player_img" alt="player">
                        </div>
                        <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                            <a class="player_search" href="http://wordpress.lan/ratingquery/">Rating Query Online</a>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                    </div>
                    <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <img src="http://wordpress.lan/wp-content/uploads/2018/08/arbiter.png"
                                 class="player_img" alt="player">
                        </div>
                        <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                            <a class="player_search" href="http://wordpress.lan/arbitercorner/">Arbiter Corner</a>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                    </div>
                    <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <img src="http://wordpress.lan/wp-content/uploads/2018/08/aicf-top.png"
                                 class="player_img" alt="player">
                        </div>
                        <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                            <a class="player_search" href="http://wordpress.lan/oci-card-holder/">PIO / OCI card
                                holders</a>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  padd_left_right_none padd_botton_10 padd_top_20">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                            <a href="http://wordpress.lan/twin-international/"> <img
                                        src="http://wordpress.lan/wp-content/uploads/2018/08/goa.jpg" alt="player"> </a>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live"
                               href="http://wordpress.lan/national-reg/">NATIONAL TOURNAMENT – <br>24 x 7 assistance is
                                arranged towards confirmation of <br> entry : Phone numbers : +917358534422 /
                                +918610193178</a></div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_5">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live"
                               href="http://wordpress.lan/aicf-payments/"> CLICK HERE FOR VARIOUS
                                PAYMENTS </a>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live"
                               href="http://wordpress.lan/faq/">FAQ</a>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_10">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                            News
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_15">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/news.jpg"
                                     alt="player">
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Under-11:
                                Shreyash and Savitha </a>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                <div class="news-date-meta"><span class="latest-news-date"><i
                                                class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/news1.jpeg"
                                     alt="player">
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href=""> National Under-07:
                                Amogh and Lakshana </a>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12
                             latest-news-meta">
                                <div class="news-date-meta"><span class="latest-news-date"><i
                                                class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/thumbnail.jpg"
                                     alt="player">
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Under-15:
                                Ajay, Divya crowned </a>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                <div class="news-date-meta"><span class="latest-news-date"><i
                                                class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                            </div>
                        </div>
                    </div>


                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                            Featured Events
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_5">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/features4.jpg"
                                     alt="player">
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href=""> 32nd
                                National Under – 11 (Open &amp; Girls)</a>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                <div class="news-date-meta"><span class="latest-news-date"><i
                                                class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/features3.jpg"
                                     alt="player">
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Cities –
                                2018 – 189826 / WB / 2018</a>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 latest-news-meta">
                                <div class="news-date-meta"><span class="latest-news-date"><i
                                                class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/features2.jpg"
                                     alt="player">
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Under – 25
                                <br> (Open &amp; Girls) – 2018</a>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                <div class="news-date-meta"><span class="latest-news-date"><i
                                                class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <img src="http://wordpress.lan/wp-content/uploads/2018/08/features1.png"
                                     alt="player">
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">Goa International GM
                                Open 2018 – 186616</a>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 latest-news-meta">
                                <div class="news-date-meta"><span class="latest-news-date"><i
                                                class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="">Title Applications</a>
                        </div>
                    </div>

                    <div class="widget-textareass padd_left_right_none col-lg-12 padd_botton_10 padd_top_10 col-md-12 col-sm-12 col-xs-12">
                        <div class="padd_left_right_none  col-lg-1 col-md-1 col-sm-1 col-xs-12">
                            <img src="http://wordpress.lan/wp-content/uploads/2018/08/india.png"
                                 class="player_img" alt="player">
                        </div>
                        <div class=" col-lg-10 col-md-10 col-sm-10 col-xs-12 padd_left_right_none">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 player_searchss"> INDIA HIGHLIGHTS</div>
                        </div>
                        <div class="padd_left_right_none  col-lg-1 col-md-1 col-sm-1 col-xs-12">
                            <img src="http://wordpress.lan/wp-content/uploads/2018/08/india.png"
                                 class="player_img" alt="player">
                        </div>
                    </div>


                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                            <div id="circle">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <strong>76612</strong>
                                </div>
                                <span>Registered Players</span>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                            <div id="circle">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <strong>28574</strong>
                                </div>
                                <span>FIDE Rated Players</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                            <div id="circle">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <strong>219</strong>
                                </div>
                                <span>Tournaments in 2018</span>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                            <div id="circle">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <strong>51</strong>
                                </div>
                                <span> Grand Masters </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_20">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                            <div id="circle">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_20">
                                    <strong>101</strong>
                                </div>
                                <span> International Masters </span>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                            <div id="circle">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_20">
                                    <strong>7</strong>
                                </div>
                                <span> Women Grand Masters </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                            AICF Chronicles
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10 events-titless " href="">July
                            2018</a>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <img src="http://wordpress.lan/wp-content/uploads/2018/08/AICF.jpg">
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
<?php
get_footer();
?>